<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumTopic extends Model
{
    use SoftDeletes;

    protected $table = 'forum_topics';

    public function posts()
    {
        return $this->hasMany(ForumPost::class,'topic_id');
    }
}
