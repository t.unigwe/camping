<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumCategory extends Model
{
    use SoftDeletes;
    protected $table = 'forum_categories';

    public function topics(){
      return  $this->hasMany(ForumTopic::class,'topic_cat');
    }
}
