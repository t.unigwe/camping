<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumPost extends Model
{
    use SoftDeletes;

    protected $table = 'forum_posts';

    public function topic()
    {
        return $this->BelongsTo(ForumTopic::class);
    }
    public function user(){
        return $this->BelongsTo(User::class);
    }
}
