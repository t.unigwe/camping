<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Course;
use App\Http\Requests\CreateAssignmentRequest;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Course $course)
    {
        return view('assignments.index', ['assignments' =>  $course->assignments, 'course' => $course]);
    }

    /**
     * Show the form for creating a new resource.
     *


     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View

     */
    public function create(Course $course)
    {
        return view('assignments.create', ['course' => $course]);
    }

    /**
     * Store a newly created resource in storage.
     *


     * @param CreateAssignmentRequest $request
     * @param Course $course
     * @return \Illuminate\Http\RedirectResponse


     */
    public function store(CreateAssignmentRequest $request, Course $course)
    {
        if (!$request->validated()) {
            return redirect()->back()->withInput($request->input())->withErrors($request);
        }
        $assignment = new Assignment();
        $assignment->course_id = $course->id;
        $assignment->name = $request['name'];
        $assignment->description = $request['description'];
        $assignment->save();
        return redirect()->route('courses.assignments.index',['course'=>$course->id]);
    }

    /**
     * Display the specified resource.
 * @param Course $course
     * @param \App\Assignment $assignment
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
 */

    public function show(Course $course ,Assignment $assignment)
    {
        return view('assignments.show', ['course' => $course, 'assignment' => $assignment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Assignment $assignment
     * @param Course $course
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateAssignmentRequest $request,Course $course, Assignment $assignment)
    {
        $assignment->name = $request['name'];
        $assignment->description = $request['description'];
        $assignment->save();
        return redirect()->route('courses.assignments.index',$course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @param Assignment $assignment
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Course $course,Assignment $assignment)
    {
        $assignment->delete();
        return redirect()->route('courses.assignments.index',['course'=>$course]);
    }
}
