@extends('layouts.dashboard')
@section ('title', 'Assignments |' . $assignment->name)
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('courses.index',$course)}}">Courses</a></li>
            <li class="breadcrumb-item"><a href="{{route('courses.assignments.index',$course)}}">Assignments</a></li>
            <li class="breadcrumb-item active" aria-current="page">Assignment overview</li>
        </ol>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-10 col-lg-6 mx-auto">
                <h1 class="display-4" style="text-align: center; margin: 50px 0;">Assignment</h1>
                <div class="form-group py-3 px-4 shadow rounded">
                    <label for="title "><h4 class="font-weight-bold">Title:</h4></label>
                    <p>{{$assignment->name}}</p>
                    <label for="Description"><h4 class="font-weight-bold">Description:</h4></label>
                    <p>{{$assignment->description}}</p>
                    <div style="padding:40px 0px;">
                        <a class="btn btn-success float-right"
                           href="{{route('courses.assignments.handin.create',[$course,$assignment])}}">Handin
                            assignment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
