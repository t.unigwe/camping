@extends('layouts.dashboard')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            @if($user->hasRole('admin'))
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">All Users</a></li>
            @else
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">{{$user->name}}</li>
        </ol>
    </nav>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 card-body text-center">
                <h1>Welcome on {{$user->name}}'s page</h1>
                @error('succes')
                <h4 class="text-success">{{ $message }}</h4>
                @enderror
            </div>

            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Edit personal information</h5>
                        <form action="{{route('users.update',$user->id)}}" method="post" class="form-group">
                            @method('PUT') @csrf
                            <input type="text" name="name" class="form-control mb-2" value="{{$user->name}}">
                            <input type="email" name="email" class="form-control" value="{{$user->email}}">
                            <button type="submit" class="btn btn-primary mt-2">Update</button>
                            @foreach($errors->all() as $error)
                                <p class="text-danger">
                                    {{ $error }}
                                </p>
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <div class="container">
                <!-- Content Row -->
                <div class="row">

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                            Tasks
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="progress progress-sm mr-2">
                                                    <div class="progress-bar bg-info" role="progressbar"
                                                         style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Progression in the courses</h6>
                            </div>
                            <div class="card-body">
                                {{--                                    @foreach($items as $item)--}}
                                <h4 class="small font-weight-bold">Account Setup <span
                                        class="float-right">Complete!</span></h4>
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%"
                                         aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                {{--                                    @endforeach--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
