@extends('layouts.dashboard')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('courses.show',[$course])}}">Course</a></li>
            <li class="breadcrumb-item"><a href="{{route('courses.assignments.show',[$course,$assignment])}}">Assignment</a></li>
            <li class="breadcrumb-item active" aria-current="page">Handins</li>
        </ol>
    </nav>
    <div class="container-sm">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">User</th>
                <th scope="col">Information</th>
                <th scope="col">File</th>
            </tr>
            </thead>
            <tbody>
            @foreach($handins as $handin)
                <tr>
                    <th scope="row">{{$handin->user->name}} ({{$handin->user->email}})</th>
                    <td>{{$handin->info}}</td>
                    <td><a class="btn btn-success" href="{{route('course.assignments.handin.download',[$course,$assignment,$handin])}}">Download</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
