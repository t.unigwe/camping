@extends('layouts.app')
@section('content')
    <!-- The Modal -->
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(array('route' => array('courses.assignments.handin.store',$course,$assignment),'method'=>'post','files'=>true)) }}
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Handin</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="card">
                    @csrf
                    @foreach($errors->all() as $error)
                        <p class="text-danger">
                            {{ $error }}
                        </p>
                    @endforeach
                    <div class="card-body">

                        {{ Form::text('info',null,array('class'=>'form-control')) }}
                        {{ Form::label('file', 'File') }}
                        {{Form::file('file')}}
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{Form::submit('Save',array('class'=>'btn btn-primary'))}}
                    {{Form::close()}}
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Sluiten</button>
                </div>
        </div>
    </div>
@endsection

